/**
 * Radius Networks, Inc.
 * http://www.radiusnetworks.com
 *
 * @author David G. Young
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package com.radiusnetworks.ibeacon.service;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.Parcelable;
import android.util.Log;

import com.radiusnetworks.ibeacon.IBeacon;
import com.radiusnetworks.ibeacon.IBeaconManager;
import com.radiusnetworks.ibeacon.Region;

/**
 * @author dyoung
 */

public class IBeaconService extends Service {
	public static final String TAG = "IBeaconService";

	private Map<Region, RangeState> rangedRegionState = new HashMap<Region, RangeState>();
	private Map<Region, MonitorState> monitoredRegionState = new HashMap<Region, MonitorState>();

	private BluetoothAdapter bluetoothAdapter;
	private boolean scanning;
	private boolean scanningPaused;
	private Date lastIBeaconDetectionTime = new Date();
	private HashSet<IBeacon> trackedBeacons;
	private Handler handler = new Handler();
	private int bindCount = 0;
	/*
	 * The scan period is how long we wait between restarting the BLE
	 * advertisement scans Each time we restart we only see the unique
	 * advertisements once (e.g. unique iBeacons) So if we want updates, we have
	 * to restart. iOS gets updates once per second, so ideally we would restart
	 * scanning that often to get the same update rate. The trouble is that when
	 * you restart scanning, it is not instantaneous, and you lose any iBeacon
	 * packets that were in the air during the restart. So the more frequently
	 * you restart, the more packets you lose. The frequency is therefore a
	 * tradeoff. Testing with 14 iBeacons, transmitting once per second, here
	 * are the counts I got for various values of the SCAN_PERIOD:
	 * 
	 * Scan period Avg iBeacons % missed 1s 6 57 2s 10 29 3s 12 14 5s 14 0
	 * 
	 * Also, because iBeacons transmit once per second, the scan period should
	 * not be an even multiple of seconds, because then it may always miss a
	 * beacon that is synchronized with when it is stopping scanning.
	 */

	private long scanPeriod = IBeaconManager.DEFAULT_FOREGROUND_SCAN_PERIOD;
	private long betweenScanPeriod = IBeaconManager.DEFAULT_FOREGROUND_BETWEEN_SCAN_PERIOD;

	private List<IBeacon> simulatedScanData = null;

	/**
	 * Class used for the client Binder. Because we know this service always
	 * runs in the same process as its clients, we don't need to deal with IPC.
	 */
	public class IBeaconBinder extends Binder {
		public IBeaconService getService() {
			Log.i(TAG, "getService of IBeaconBinder called");
			// Return this instance of LocalService so clients can call public
			// methods
			return IBeaconService.this;
		}
	}

	/**
	 * Command to the service to display a message
	 */
	
	public static final int MSG_START_SCANNING = 99;
	public static final int MSG_START_RANGING = 2;
	public static final int MSG_STOP_RANGING = 3;
	public static final int MSG_START_MONITORING = 4;
	public static final int MSG_STOP_MONITORING = 5;
	public static final int MSG_SET_SCAN_PERIODS = 6;


	static class IncomingHandler extends Handler {
		private final WeakReference<IBeaconService> mService;

		IncomingHandler(IBeaconService service) {
			mService = new WeakReference<IBeaconService>(service);
		}

		@Override
		public void handleMessage(Message msg) {
			IBeaconService service = mService.get();
			StartRMData startRMData = (StartRMData) msg.obj;

			if (service != null) {
				switch (msg.what) {
				case MSG_START_RANGING:
					Log.i(TAG, "start ranging received");
					service.startRangingBeaconsInRegion(startRMData
							.getRegionData(),
							new com.radiusnetworks.ibeacon.service.Callback(
									startRMData.getCallbackPackageName()));
					service.setScanPeriods(startRMData.getScanPeriod(),
							startRMData.getBetweenScanPeriod());
					break;
				case MSG_STOP_RANGING:
					Log.i(TAG, "stop ranging received");
					service.stopRangingBeaconsInRegion(startRMData.getRegionData());
					service.setScanPeriods(startRMData.getScanPeriod(),	startRMData.getBetweenScanPeriod());
					break;
				case MSG_START_MONITORING:
					Log.i(TAG, "start monitoring received");
					service.startMonitoringBeaconsInRegion(startRMData
							.getRegionData(),
							new com.radiusnetworks.ibeacon.service.Callback(
									startRMData.getCallbackPackageName()));
					service.setScanPeriods(startRMData.getScanPeriod(),
							startRMData.getBetweenScanPeriod());
					break;
				case MSG_STOP_MONITORING:
					Log.i(TAG, "stop monitoring received");
					service.stopMonitoringBeaconsInRegion(startRMData
							.getRegionData());
					service.setScanPeriods(startRMData.getScanPeriod(),
							startRMData.getBetweenScanPeriod());
					break;
				case MSG_SET_SCAN_PERIODS:
					Log.i(TAG, "set scan intervals received");
					service.setScanPeriods(startRMData.getScanPeriod(),
							startRMData.getBetweenScanPeriod());
					break;

				// Shahbaz
					
				case MSG_START_SCANNING:
					Log.i(TAG, "Starting Scanning");
					service.startScan();
					break;
					

					
					
				case MSG_CONNECT_GATT:
					Log.i(TAG, "Connect to Gatt Server received");
					service.connect(startRMData.getDeviceAddress());
					break;
					
				// Shahbaz
				case MSG_STOP_READING_CHARACTERISTIC:
					Log.i(TAG, "Stopping reading Characteristics");
					service.reading = false;
					break;
					
				// Shahbaz
				case MSG_WRITE_CHARACTERISTIC:
					Log.i(TAG, "Writing Characteristics");
					service.reading = false;
					service.writeCharacteristic(startRMData.getCharacteristicData());
					break;

				default:
					super.handleMessage(msg);
				}
			}
		}
	}

	/**
	 * Target we publish for clients to send messages to IncomingHandler.
	 */
	final Messenger mMessenger = new Messenger(new IncomingHandler(this));

	/**
	 * When binding to the service, we return an interface to our messenger for
	 * sending messages to the service.
	 */
	@Override
	public IBinder onBind(Intent intent) {
		Log.i(TAG, "binding");
		bindCount++;
		return mMessenger.getBinder();
	}

	@Override
	public boolean onUnbind(Intent intent) {
		Log.i(TAG, "unbinding");
		bindCount--;
		return false;
	}

	@Override
	public void onCreate() {
		Log.i(TAG, "iBeaconService version 0.7.5 is starting up");
		getBluetoothAdapter();

		// Look for simulated scan data
		try {
			Class klass = Class
					.forName("com.radiusnetworks.ibeacon.SimulatedScanData");
			java.lang.reflect.Field f = klass.getField("iBeacons");
			this.simulatedScanData = (List<IBeacon>) f.get(null);
		} catch (ClassNotFoundException e) {
			if (IBeaconManager.LOG_DEBUG)
				Log.d(TAG,
						"No com.radiusnetworks.ibeacon.SimulatedScanData class exists.");
		} catch (Exception e) {
			Log.e(TAG,
					"Cannot get simulated Scan data.  Make sure your com.radiusnetworks.ibeacon.SimulatedScanData class defines a field with the signature 'public static List<IBeacon> iBeacons'",
					e);
		}
	}

	@Override
	public void onDestroy() {
		Log.i(TAG, "onDestory called.  stopping scanning");
		handler.removeCallbacksAndMessages(null);
		scanLeDevice(false);
		if (bluetoothAdapter != null) {
			bluetoothAdapter.stopLeScan(leScanCallback);
			lastScanEndTime = new Date().getTime();
		}
	}

	private int ongoing_notification_id = 1;

	/*
	 * Returns true if the service is running, but all bound clients have
	 * indicated they are in the background
	 */
	private boolean isInBackground() {
		if (IBeaconManager.LOG_DEBUG)
			Log.d(TAG, "bound client count:" + bindCount);
		return bindCount == 0;
	}

	/**
	 * methods for clients
	 */

	public void startRangingBeaconsInRegion(Region region, Callback callback) {
		synchronized (rangedRegionState) {
			if (rangedRegionState.containsKey(region)) {
				Log.i(TAG,
						"Already ranging that region -- will replace existing region.");
				rangedRegionState.remove(region); // need to remove it,
													// otherwise the old object
													// will be retained because
													// they are .equal
			}
			rangedRegionState.put(region, new RangeState(callback));
		}
		if (!scanning) {
			scanLeDevice(true);
		}
	}

	public void stopRangingBeaconsInRegion(Region region) {
		synchronized (rangedRegionState) {
			rangedRegionState.remove(region);
		}
		if (scanning && rangedRegionState.size() == 0
				&& monitoredRegionState.size() == 0) {
			scanLeDevice(false);
		}
	}

	public void startMonitoringBeaconsInRegion(Region region, Callback callback) {
		if (IBeaconManager.LOG_DEBUG)
			Log.d(TAG, "startMonitoring called");
		synchronized (monitoredRegionState) {
			if (monitoredRegionState.containsKey(region)) {
				Log.i(TAG,
						"Already monitoring that region -- will replace existing region monitor.");
				monitoredRegionState.remove(region); // need to remove it,
														// otherwise the old
														// object will be
														// retained because they
														// are .equal
			}
			monitoredRegionState.put(region, new MonitorState(callback));
		}
		if (IBeaconManager.LOG_DEBUG)
			Log.d(TAG, "Currently monitoring " + monitoredRegionState.size()
					+ " regions.");
		if (!scanning) {
			scanLeDevice(true);
		}
	}

	public void stopMonitoringBeaconsInRegion(Region region) {
		if (IBeaconManager.LOG_DEBUG)
			Log.d(TAG, "stopMonitoring called");
		synchronized (monitoredRegionState) {
			monitoredRegionState.remove(region);
		}
		if (IBeaconManager.LOG_DEBUG)
			Log.d(TAG, "Currently monitoring " + monitoredRegionState.size()
					+ " regions.");
		if (scanning && rangedRegionState.size() == 0
				&& monitoredRegionState.size() == 0) {
			scanLeDevice(false);
		}
	}

	public void setScanPeriods(long scanPeriod, long betweenScanPeriod) {
		this.scanPeriod = scanPeriod;
		this.betweenScanPeriod = betweenScanPeriod;
		long now = new Date().getTime();
		if (nextScanStartTime > now) {
			// We are waiting to start scanning. We may need to adjust the next
			// start time
			// only do an adjustment if we need to make it happen sooner.
			// Otherwise, it will
			// take effect on the next cycle.
			long proposedNextScanStartTime = (lastScanEndTime + betweenScanPeriod);
			if (proposedNextScanStartTime < nextScanStartTime) {
				nextScanStartTime = proposedNextScanStartTime;
				Log.i(TAG, "Adjusted nextScanStartTime to be "
						+ new Date(nextScanStartTime));
			}
		}
		if (scanStopTime > now) {
			// we are waiting to stop scanning. We may need to adjust the stop
			// time
			// only do an adjustment if we need to make it happen sooner.
			// Otherwise, it will
			// take effect on the next cycle.
			long proposedScanStopTime = (lastScanStartTime + scanPeriod);
			if (proposedScanStopTime < scanStopTime) {
				scanStopTime = proposedScanStopTime;
				Log.i(TAG, "Adjusted scanStopTime to be "
						+ new Date(scanStopTime));
			}
		}
	}

	private long lastScanStartTime = 0l;
	private long lastScanEndTime = 0l;
	private long nextScanStartTime = 0l;
	private long scanStopTime = 0l;

	private void scanLeDevice(final Boolean enable) {
		if (getBluetoothAdapter() == null) {
			Log.e(TAG, "No bluetooth adapter.  iBeaconService cannot scan.");
			if (simulatedScanData == null) {
				Log.w(TAG, "exiting");
				return;
			} else {
				Log.w(TAG, "proceeding with simulated scan data");
			}
		}
		if (enable) {
			long millisecondsUntilStart = nextScanStartTime
					- (new Date().getTime());
			if (millisecondsUntilStart > 0) {
				if (IBeaconManager.LOG_DEBUG)
					Log.d(TAG,
							"Waiting to start next bluetooth scan for another "
									+ millisecondsUntilStart + " milliseconds");
				// Don't actually wait until the next scan time -- only wait up
				// to 1 second. this
				// allows us to start scanning sooner if a consumer enters the
				// foreground and expects
				// results more quickly
				handler.postDelayed(new Runnable() {
					@Override
					public void run() {
						scanLeDevice(true);
					}
				}, millisecondsUntilStart > 1000 ? 1000
						: millisecondsUntilStart);
				return;
			}

			trackedBeacons = new HashSet<IBeacon>();
			if (scanning == false || scanningPaused == true) {
				scanning = true;
				scanningPaused = false;
				try {
					if (getBluetoothAdapter() != null) {
						if (getBluetoothAdapter().isEnabled()) {
							getBluetoothAdapter().startLeScan(leScanCallback);
							lastScanStartTime = new Date().getTime();
						} else {
							Log.w(TAG,
									"Bluetooth is disabled.  Cannot scan for iBeacons.");
						}
					}
				} catch (Exception e) {
					Log.e("TAG",
							"Exception starting bluetooth scan.  Perhaps bluetooth is disabled or unavailable?");
				}
			} else {
				if (IBeaconManager.LOG_DEBUG)
					Log.d(TAG, "We are already scanning");
			}
			scanStopTime = (new Date().getTime() + scanPeriod);
			scheduleScanStop();

			if (IBeaconManager.LOG_DEBUG)
				Log.d(TAG, "Scan started");
		} else {
			if (IBeaconManager.LOG_DEBUG)
				Log.d(TAG, "disabling scan");
			scanning = false;
			if (getBluetoothAdapter() != null) {
				getBluetoothAdapter().stopLeScan(leScanCallback);
				lastScanEndTime = new Date().getTime();
			}
		}
	}

	private void scheduleScanStop() {
		// Stops scanning after a pre-defined scan period.
		long millisecondsUntilStop = scanStopTime - (new Date().getTime());
		if (millisecondsUntilStop > 0) {
			if (IBeaconManager.LOG_DEBUG)
				Log.d(TAG, "Waiting to stop scan for another "
						+ millisecondsUntilStop + " milliseconds");
			handler.postDelayed(new Runnable() {
				@Override
				public void run() {
					scheduleScanStop();
				}
			}, millisecondsUntilStop > 1000 ? 1000 : millisecondsUntilStop);
		} else {
			finishScanCycle();
		}

	}

	private void finishScanCycle() {
		if (IBeaconManager.LOG_DEBUG)
			Log.d(TAG, "Done with scan cycle");
		processExpiredMonitors();
		if (scanning == true) {
			if (!anyRangingOrMonitoringRegionsActive()) {
				if (IBeaconManager.LOG_DEBUG)
					Log.d(TAG,
							"Not starting scan because no monitoring or ranging regions are defined.");
			} else {
				processRangeData();
				if (IBeaconManager.LOG_DEBUG)
					Log.d(TAG,
							"Restarting scan.  Unique beacons seen last cycle: "
									+ trackedBeacons.size());
				if (getBluetoothAdapter() != null) {
					if (getBluetoothAdapter().isEnabled()) {
						getBluetoothAdapter().stopLeScan(leScanCallback);
						lastScanEndTime = new Date().getTime();
					} else {
						Log.w(TAG,
								"Bluetooth is disabled.  Cannot scan for iBeacons.");
					}
				}

				scanningPaused = true;
				// If we want to use simulated scanning data, do it here. This
				// is used for testing in an emulator
				if (simulatedScanData != null) {
					// if simulatedScanData is provided, it will be seen every
					// scan cycle. *in addition* to anything actually seen in
					// the air
					// it will not be used if we are not in debug mode
					if (0 != (getApplicationInfo().flags &= ApplicationInfo.FLAG_DEBUGGABLE)) {
						for (IBeacon iBeacon : simulatedScanData) {
							processIBeaconFromScan(iBeacon);
						}
					} else {
						Log.w(TAG,
								"Simulated scan data provided, but ignored because we are not running in debug mode.  Please remove simulated scan data for production.");
					}
				}
				nextScanStartTime = (new Date().getTime() + betweenScanPeriod);
				scanLeDevice(true);
			}
		}
	}

	// Device scan callback.
	private BluetoothAdapter.LeScanCallback leScanCallback = new BluetoothAdapter.LeScanCallback() {

		@Override
		public void onLeScan(final BluetoothDevice device, final int rssi,
				final byte[] scanRecord) {
			if (IBeaconManager.LOG_DEBUG)
				Log.d(TAG, "got record");
			new ScanProcessor().execute(new ScanData(device, rssi, scanRecord));

		}
	};

	private class ScanData {
		public ScanData(BluetoothDevice device, int rssi, byte[] scanRecord) {
			this.device = device;
			this.rssi = rssi;
			this.scanRecord = scanRecord;
		}

		@SuppressWarnings("unused")
		public BluetoothDevice device;
		public int rssi;
		public byte[] scanRecord;
	}

	private void processRangeData() {
		Iterator<Region> regionIterator = rangedRegionState.keySet().iterator();
		while (regionIterator.hasNext()) {
			Region region = regionIterator.next();
			RangeState rangeState = rangedRegionState.get(region);
			if (IBeaconManager.LOG_DEBUG)
				Log.d(TAG, "Calling ranging callback with "
						+ rangeState.getIBeacons().size() + " iBeacons");
			rangeState.getCallback().call(IBeaconService.this, "rangingData",
					new RangingData(rangeState.getIBeacons(), region));
			rangeState.clearIBeacons();
		}

	}

	private void processExpiredMonitors() {
		Iterator<Region> monitoredRegionIterator = monitoredRegionState
				.keySet().iterator();
		while (monitoredRegionIterator.hasNext()) {
			Region region = monitoredRegionIterator.next();
			MonitorState state = monitoredRegionState.get(region);
			if (state.isNewlyOutside()) {
				if (IBeaconManager.LOG_DEBUG)
					Log.d(TAG, "found a monitor that expired: " + region);
				state.getCallback().call(IBeaconService.this, "monitoringData",
						new MonitoringData(state.isInside(), region));
			}
		}
	}

	
	
	ArrayList<Parcelable> beaconArray=new ArrayList<Parcelable>();
	
	private void processIBeaconFromScan(IBeacon iBeacon) {
		lastIBeaconDetectionTime = new Date();
		trackedBeacons.add(iBeacon);
		
		//Shahbaz
		//Simply show a list of all beacons for "Editing"
		IBeaconData bd = new IBeaconData(iBeacon);
		bd.setDevice(iBeacon.getDevice());
		
		if(!beaconArray.contains(bd)){
			Intent intent =new Intent(ACTION_BEACONS_FOUND);
			beaconArray.add(bd);
			intent.putParcelableArrayListExtra("beacons", beaconArray);
			sendBroadcast(intent);
		}
		//Shahbaz
		
		
		
		if (IBeaconManager.LOG_DEBUG)
			Log.d(TAG, "iBeacon detected :" + iBeacon.getProximityUuid() + " "
					+ iBeacon.getMajor() + " " + iBeacon.getMinor()
					+ " accuracy: " + iBeacon.getAccuracy() + " proximity: "
					+ iBeacon.getProximity());

		List<Region> matchedRegions = null;
		synchronized (monitoredRegionState) {
			matchedRegions = matchingRegions(iBeacon,
					monitoredRegionState.keySet());
		}
		Iterator<Region> matchedRegionIterator = matchedRegions.iterator();
		while (matchedRegionIterator.hasNext()) {
			Region region = matchedRegionIterator.next();
			MonitorState state = monitoredRegionState.get(region);
			if (state.markInside()) {
				state.getCallback().call(IBeaconService.this, "monitoringData",
						new MonitoringData(state.isInside(), region));
			}
		}

		if (IBeaconManager.LOG_DEBUG)
			Log.d(TAG, "looking for ranging region matches for this ibeacon");
		synchronized (rangedRegionState) {
			matchedRegions = matchingRegions(iBeacon,
					rangedRegionState.keySet());
		}
		matchedRegionIterator = matchedRegions.iterator();
		while (matchedRegionIterator.hasNext()) {
			Region region = matchedRegionIterator.next();
			if (IBeaconManager.LOG_DEBUG)
				Log.d(TAG, "matches ranging region: " + region);
			RangeState rangeState = rangedRegionState.get(region);
			rangeState.addIBeacon(iBeacon);
		}
	}

	private class ScanProcessor extends AsyncTask<ScanData, Void, Void> {

		@Override
		protected Void doInBackground(ScanData... params) {
			ScanData scanData = params[0];

			IBeacon iBeacon = IBeacon.fromScanData(scanData.scanRecord,
					scanData.rssi, scanData.device);
			if (iBeacon != null) {
				// ==================================================
				// Shahbaz
				// ==================================================
				iBeacon.setDevice(scanData.device);
				//iBeacon.setMacAddress(scanData.device.getAddress());
				// ==================================================
				// Shahbaz
				// ==================================================

				processIBeaconFromScan(iBeacon);
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
		}

		@Override
		protected void onPreExecute() {
		}

		@Override
		protected void onProgressUpdate(Void... values) {
		}
	}

	private List<Region> matchingRegions(IBeacon iBeacon,
			Collection<Region> regions) {
		List<Region> matched = new ArrayList<Region>();
		Iterator<Region> regionIterator = regions.iterator();
		while (regionIterator.hasNext()) {
			Region region = regionIterator.next();
			if (region.matchesIBeacon(iBeacon)) {
				matched.add(region);
			} else {
				if (IBeaconManager.LOG_DEBUG)
					Log.d(TAG, "This region does not match: " + region);
			}

		}
		return matched;
	}

	/*
	 * Returns false if no ranging or monitoring regions have beeen requested.
	 * This is useful in determining if we should scan at all.
	 */
	private boolean anyRangingOrMonitoringRegionsActive() {
		return (rangedRegionState.size() + monitoredRegionState.size()) > 0;
	}

	private BluetoothAdapter getBluetoothAdapter() {
		if (bluetoothAdapter == null) {
			// Initializes Bluetooth adapter.
			final BluetoothManager bluetoothManager = (BluetoothManager) this
					.getApplicationContext().getSystemService(
							Context.BLUETOOTH_SERVICE);
			bluetoothAdapter = bluetoothManager.getAdapter();
		}
		return bluetoothAdapter;
	}

	// ======================================================================
	// SHAHBAZ ADDED CODE HERE
	// ======================================================================

	// SHAHBAZ
	public static final int MSG_CONNECT_GATT = 7;
	public static final int MSG_STOP_READING_CHARACTERISTIC = 8;
	public static final int MSG_WRITE_CHARACTERISTIC = 9;

	
	
	private static final int STATE_DISCONNECTED = 0;
	private static final int STATE_CONNECTING = 1;
	private static final int STATE_CONNECTED = 2;

	private static final String DEVICE_INFORMATION_GATT_SERVICE = "0000fff0-0000-1000-8000-00805f9b34fb";
	private static final String DEVICE_CHARACTERISTIC_UUID = "0000fff1-0000-1000-8000-00805f9b34fb";
	private static final String DEVICE_CHARACTERISTIC_MAJOR = "0000fff2-0000-1000-8000-00805f9b34fb";
	private static final String DEVICE_CHARACTERISTIC_MINOR = "0000fff3-0000-1000-8000-00805f9b34fb";

	private BluetoothGattService deviceInfoService;
	private ArrayList<BluetoothGattCharacteristic> deviceCharacteristics = new ArrayList<BluetoothGattCharacteristic>();
	private ArrayList<HashMap<String, String>> deviceCharacteristicsData = new ArrayList<HashMap<String, String>>();

	private Map<BluetoothGattCharacteristic, CharacteristicData> characteristicData = new HashMap<BluetoothGattCharacteristic, CharacteristicData>();

	public final static String ACTION_GATT_CONNECTED = "com.example.bluetooth.le.ACTION_GATT_CONNECTED";
	public final static String ACTION_GATT_DISCONNECTED = "com.example.bluetooth.le.ACTION_GATT_DISCONNECTED";
	public final static String ACTION_GATT_SERVICES_DISCOVERED = "com.example.bluetooth.le.ACTION_GATT_SERVICES_DISCOVERED";
//	public final static String ACTION_DATA_AVAILABLE = "com.example.bluetooth.le.ACTION_DATA_AVAILABLE";
	
	public final static String ACTION_BEACONS_FOUND = "com.example.bluetooth.le.ACTION_BEACONS_FOUNDS";
	public final static String CHARACTERISTIC_UUID_FOUND = "com.example.bluetooth.le.CHARACTERISTIC_UUID_FOUND";

	private String mBluetoothDeviceAddress;
	private BluetoothGatt mBluetoothGatt;
	private int mConnectionState = STATE_DISCONNECTED;
	
	private boolean reading=true;
	
	private BluetoothGattCharacteristic selCharacteristic;
	
	
	
	
	
	
	/**
	 * Method to start scanning devices.
	 */

	public void startScan() {
		if (!scanning) {
			scanLeDevice(true);
		}
	}

	
	
	
	
	
	
	
	
	

	/**
	 * Connects to the GATT server hosted on the Bluetooth LE device.
	 * 
	 * @param address
	 *            The device address of the destination device.
	 * 
	 * @return Return true if the connection is initiated successfully. The
	 *         connection result is reported asynchronously through the
	 *         {@code BluetoothGattCallback#onConnectionStateChange(android.bluetooth.BluetoothGatt, int, int)}
	 *         callback.
	 */
	public boolean connect(final String address) {
		mBluetoothDeviceAddress = address;

		if (getBluetoothAdapter() == null || address == null) {
			Log.w(TAG,
					"BluetoothAdapter not initialized or unspecified address.");
			return false;
		}

		// Previously connected device. Try to reconnect.
		if (mBluetoothDeviceAddress != null && address.equals(mBluetoothDeviceAddress) && mBluetoothGatt != null) {
			Log.d(TAG,	"Trying to use an existing mBluetoothGatt for connection.");
			if (mBluetoothGatt.connect()) {
				mConnectionState = STATE_CONNECTING;
				return true;
			} else {
				return false;
			}
		}

		final BluetoothDevice device = getBluetoothAdapter().getRemoteDevice(mBluetoothDeviceAddress);
		if (device == null) {
			Log.w(TAG, "Device not found.  Unable to connect.");
			return false;
		}
		// We want to directly connect to the device, so we are setting the
		// autoConnect
		// parameter to false.
		mBluetoothGatt = device.connectGatt(this.getApplicationContext(), true, mGattCallback);

		Log.d(TAG, "Trying to create a new connection.");
		mConnectionState = STATE_CONNECTING;
		return true;
	}

	/**
	 * Disconnects an existing connection or cancel a pending connection. The
	 * disconnection result is reported asynchronously through the
	 * {@code BluetoothGattCallback#onConnectionStateChange(android.bluetooth.BluetoothGatt, int, int)}
	 * callback.
	 */
	public void disconnect() {
		if (getBluetoothAdapter() == null || mBluetoothGatt == null) {
			Log.w(TAG, "BluetoothAdapter not initialized");
			return;
		}
		mBluetoothGatt.disconnect();
	}

	// Implements callback methods for GATT events that the app cares about. For
	// example,
	// connection change and services discovered.
	private final BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {
		@Override
		public void onConnectionStateChange(BluetoothGatt gatt, int status,
				int newState) {
			String intentAction;
			if (newState == BluetoothProfile.STATE_CONNECTED) {
				intentAction = ACTION_GATT_CONNECTED;
				mConnectionState = STATE_CONNECTED;
				broadcastUpdate(intentAction);
				
				Log.i(TAG, "Connected to GATT server.");

				// Attempts to discover services after successful connection.
				Log.i(TAG, "Attempting to start service discovery:"	+ mBluetoothGatt.discoverServices());

			} else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
				intentAction = ACTION_GATT_DISCONNECTED;
				mConnectionState = STATE_DISCONNECTED;
				Log.i(TAG, "Disconnected from GATT server.");
				broadcastUpdate(intentAction);
			}
		}

		@Override
		public void onServicesDiscovered(BluetoothGatt gatt, int status) {
			//First we disable scanning service
			scanLeDevice(false);
			
			if (status == BluetoothGatt.GATT_SUCCESS) {
				parseGattServices(gatt.getServices());
			} else {
				Log.w(TAG, "onServicesDiscovered received: " + status);
			}
		}

		@Override
		public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
			//If we don't want to keep reading characteristics.
			if (status == BluetoothGatt.GATT_SUCCESS) {
	            final byte[] data = characteristic.getValue();
	            if (data != null && data.length > 0) {
	                String newstr=IBeacon.bytesToHex(data);
	                CharacteristicData d = new CharacteristicData();//(characteristic);
	                d.proximityUuid = newstr;
	                
	                //Keep reference of last read characteristice for writing in furture.
	                selCharacteristic = characteristic;
	                
					final Intent intent = new Intent(CHARACTERISTIC_UUID_FOUND);
	                intent.putExtra(CHARACTERISTIC_UUID_FOUND, d);
			        sendBroadcast(intent);
		        }
			}
				
		}

		public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
			if (status == BluetoothGatt.GATT_SUCCESS) {
				// broadcastUpdate(ACTION_DATA_AVAILABLE, characteristic);
			}
		};

		@Override
		public void onCharacteristicChanged(BluetoothGatt gatt,
				BluetoothGattCharacteristic characteristic) {
			// broadcastUpdate(ACTION_DATA_AVAILABLE, characteristic);
		}
	};

	// Demonstrates how to iterate through the supported GATT
	// Services/Characteristics.
	private void parseGattServices(List<BluetoothGattService> gattServices) {
		if (gattServices == null)return;
		
		String uuid = null;

		// Loops through available GATT Services.
		for (BluetoothGattService gattService : gattServices) {
			uuid = gattService.getUuid().toString();
			// @Shahbaz
			// For Accent Sytems beacons, we need to only retrieve
			// "Device Information Service"
			// This service is having uuid
			// "0000fff0-0000-1000-8000-00805f9b34fb"
			// So we only use this service in the UI.
			if (uuid.equals(DEVICE_INFORMATION_GATT_SERVICE)) {
				// Set reference of Device Information Gatt Service.
				deviceInfoService = gattService;

				// Fetch all characteristics.
				List<BluetoothGattCharacteristic> gattCharacteristics = gattService
						.getCharacteristics();

				// Loops through available Characteristics.
				for (BluetoothGattCharacteristic gattCharacteristic : gattCharacteristics) {
					uuid = gattCharacteristic.getUuid().toString();
					// Filter only UUID, Major and Minor characteristics.
					if (uuid.equals(DEVICE_CHARACTERISTIC_UUID)){/*
							|| uuid.equals(DEVICE_CHARACTERISTIC_MAJOR)
							|| uuid.equals(DEVICE_CHARACTERISTIC_MINOR)) {*/
						if (!deviceCharacteristics.contains(gattCharacteristic)) {
							deviceCharacteristics.add(gattCharacteristic);
							mBluetoothGatt.setCharacteristicNotification(gattCharacteristic, true);
							mBluetoothGatt.readCharacteristic(gattCharacteristic);
						}
					}
				}
			}
		}
	}
	
	
	public void writeCharacteristic(final CharacteristicData d) {
		if(mBluetoothGatt!=null && selCharacteristic!=null){
			final int charaProp = selCharacteristic.getProperties();
            if ((charaProp | BluetoothGattCharacteristic.PROPERTY_READ) > 0) {
                // If there is an active notification on a characteristic, clear
                // it first so it doesn't update the data field on the user interface.
            	mBluetoothGatt.setCharacteristicNotification(selCharacteristic, false);
            }
             
            
            
        	mBluetoothGatt.setCharacteristicNotification(selCharacteristic, true);
        	
        	selCharacteristic.setValue(IBeacon.hexStringToByteArray(d.getUUID()));
        	mBluetoothGatt.writeCharacteristic(selCharacteristic);
		}
	}
	

	private void broadcastUpdate(final String action) {
		final Intent intent = new Intent(action);
		sendBroadcast(intent);
	}
	
	
	
	
}

/**
 * This class includes a small subset of standard GATT attributes for
 * demonstration purposes.
 */
class SampleGattAttributes {
	private static HashMap<String, String> attributes = new HashMap();

	// Service UUID for Device Information
	public static String DEVICE_INFORMATION = "0000fff0-0000-1000-8000-00805f9b34fb";

	// Characteristic UUIDs for DEVICE_INFORMATION Service.
	public static String UUID = "0000fff1-0000-1000-8000-00805f9b34fb";
	public static String MAJOR = "0000fff2-0000-1000-8000-00805f9b34fb";
	public static String MINOR = "0000fff3-0000-1000-8000-00805f9b34fb";

	static {
		// Sample Services.
		attributes.put(DEVICE_INFORMATION, "Device Information");

		// Sample Characteristics.
		attributes.put(UUID, "UUID");
		attributes.put(MAJOR, "Major");
		attributes.put(MINOR, "Minor");
	}

	public static String lookup(String uuid, String defaultName) {
		String name = attributes.get(uuid);
		return name == null ? defaultName : name;
	}
}
