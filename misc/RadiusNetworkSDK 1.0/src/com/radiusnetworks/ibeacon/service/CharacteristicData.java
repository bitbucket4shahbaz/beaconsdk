/**
 * Radius Networks, Inc.
 * http://www.radiusnetworks.com
 * 
 * @author David G. Young
 * 
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package com.radiusnetworks.ibeacon.service;

import android.bluetooth.BluetoothGattCharacteristic;
import android.os.Parcel;
import android.os.Parcelable;

public class CharacteristicData implements Parcelable {
//	protected Integer major = 0;
//	protected Integer minor = 0;
	protected String proximityUuid = "";
	protected BluetoothGattCharacteristic chara = null;
	
    @Override
	public int describeContents() {
		return 0;
	}
    
    public void setUUID(String val){
    	this.proximityUuid = val;
    }
    
    public void setCharacteristic(BluetoothGattCharacteristic c){
    	this.chara = c;
    }
    
    
    public BluetoothGattCharacteristic getCharacteristic(){
    	return this.chara;
    }
    
    public String getUUID(){
    	return this.proximityUuid;
    }
    
    public void writeToParcel(Parcel out, int flags) {
        out.writeString(proximityUuid);
    }

    public static final Parcelable.Creator<CharacteristicData> CREATOR   = new Parcelable.Creator<CharacteristicData>() {
        public CharacteristicData createFromParcel(Parcel in) {
            return new CharacteristicData(in);
        }

        public CharacteristicData[] newArray(int size) {
            return new CharacteristicData[size];
        }
    };
    
    private CharacteristicData(Parcel in) { 
	   	 proximityUuid = in.readString();
    }

	public CharacteristicData() {
		// TODO Auto-generated constructor stub
	}
}
