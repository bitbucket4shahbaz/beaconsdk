/**
 * BeaconWatcher Technologies
 * http://www.beaconwatcher.com
 * 
 * @author Shahbaz Ali
 *
 */

package com.beaconwatcher.ibeacon;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.R;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.util.Log;


/**
 * An class used to interact with IBeaconManager,
 * it's sole purpose is to connect to IBeacon for changing it's settings.
 * @author Shahbaz Ali
 *
 */
public class IBeaconConnectionManager{
	  public static final String EXTRAS_DEVICE_NAME = "DEVICE_NAME";
	  public static final String EXTRAS_DEVICE_ADDRESS = "DEVICE_ADDRESS";
	  
	  protected static IBeaconConnectionManager client = null;
	  private Context context;
	  
	  String mDeviceAddress = "";
	  
	  private BluetoothLeService mBluetoothLeService;
	  private ArrayList<ArrayList<BluetoothGattCharacteristic>> mGattCharacteristics = new ArrayList<ArrayList<BluetoothGattCharacteristic>>();

	  
	  private boolean mConnected = false;
	  private BluetoothGattCharacteristic mNotifyCharacteristic;
	  
	  
	  /**
	     * set to true if you want to see debug messages associated with this library
	     */
	  public static boolean LOG_DEBUG = false;
	  private static final String TAG = "IBeaconConnectionManager";

	
	  /**
		 * An accessor for the singleton instance of this class.  A context must be provided, but if you need to use it from a non-Activity
		 * or non-Service class, you can attach it to another singleton or a subclass of the Android Application class.
		 */
		public static IBeaconConnectionManager getInstanceForApplication(Context context) {
			if (client == null) {
				if (IBeaconConnectionManager.LOG_DEBUG) Log.d(TAG, "IBeaconConnectionManager instance creation");
				client = new IBeaconConnectionManager(context);
			}
			return client;
		}
		
		protected IBeaconConnectionManager(Context context) {
			this.context = context;
		}
		
		public void connectToBeacon(String address){
			context.registerReceiver(mGattUpdateReceiver, makeGattUpdateIntentFilter());
			Intent gattServiceIntent = new Intent(context, BluetoothLeService.class);
			context.bindService(gattServiceIntent, mServiceConnection, context.BIND_AUTO_CREATE);
		}
	  
	  
	  
	  
	// Demonstrates how to iterate through the supported GATT Services/Characteristics.
	    // In this sample, we populate the data structure that is bound to the ExpandableListView
	    // on the UI.
	    private void displayGattServices(List<BluetoothGattService> gattServices) {
	        if (gattServices == null) return;
	        String uuid = null;
	        ArrayList<HashMap<String, String>> gattServiceData = new ArrayList<HashMap<String, String>>();
	        ArrayList<ArrayList<HashMap<String, String>>> gattCharacteristicData = new ArrayList<ArrayList<HashMap<String, String>>>();
	        
	        mGattCharacteristics = new ArrayList<ArrayList<BluetoothGattCharacteristic>>();

	        // Loops through available GATT Services.
	        for (BluetoothGattService gattService : gattServices) {
	            HashMap<String, String> currentServiceData = new HashMap<String, String>();
	            uuid = gattService.getUuid().toString();
	            
	            
	            //@Shahbaz
	            //For Accent Sytems beacons, we need to only retrieve "Device Information Service"
	            //This service is having uuid "0000fff0-0000-1000-8000-00805f9b34fb"
	            //So we only this service in the UI.
	            
/*	            currentServiceData.put(LIST_NAME, SampleGattAttributes.lookup(uuid, unknownServiceString));
				currentServiceData.put(LIST_UUID, uuid);*/
                gattServiceData.add(currentServiceData);

                ArrayList<HashMap<String, String>> gattCharacteristicGroupData =new ArrayList<HashMap<String, String>>();
                List<BluetoothGattCharacteristic> gattCharacteristics = gattService.getCharacteristics();
                ArrayList<BluetoothGattCharacteristic> charas = new ArrayList<BluetoothGattCharacteristic>();

                // Loops through available Characteristics.
                for (BluetoothGattCharacteristic gattCharacteristic : gattCharacteristics) {
                    charas.add(gattCharacteristic);
                    HashMap<String, String> currentCharaData = new HashMap<String, String>();
                    uuid = gattCharacteristic.getUuid().toString();

                    //@Shahbaz
                    //Filter only UUID, Major and Minor characteristics.
                    /*if(!SampleGattAttributes.lookup(uuid, unknownCharaString).equals(unknownCharaString)){
	                    currentCharaData.put( LIST_NAME, SampleGattAttributes.lookup(uuid, unknownCharaString));
	                    Log.d(TAG, gattCharacteristic.getService().getUuid().toString()+"|||||"+uuid.toString());
	                    currentCharaData.put(LIST_UUID, uuid);
	                    gattCharacteristicGroupData.add(currentCharaData);
                    }*/
                }
                mGattCharacteristics.add(charas);
               // gattCharacteristicData.add(gattCharacteristicGroupData);
	        }
	    }


	  
	  
	  
	  
	// Code to manage Service lifecycle.
	    private final ServiceConnection mServiceConnection = new ServiceConnection() {

	        @Override
	        public void onServiceConnected(ComponentName componentName, IBinder service) {
	            mBluetoothLeService = ((BluetoothLeService.LocalBinder) service).getService();
	            if (!mBluetoothLeService.initialize()) {
//	                finish();
	            }
	            // Automatically connects to the device upon successful start-up initialization.
	            mBluetoothLeService.connect(mDeviceAddress);
	        }

	        @Override
	        public void onServiceDisconnected(ComponentName componentName) {
	            mBluetoothLeService = null;
	        }
	    };
	    
	    public void setCharacteristicNotification(BluetoothGattCharacteristic chara, Boolean b){
	    	mBluetoothLeService.setCharacteristicNotification(chara, b);
	    }
	    
	    
	 // Handles various events fired by the Service.
	    // ACTION_GATT_CONNECTED: connected to a GATT server.
	    // ACTION_GATT_DISCONNECTED: disconnected from a GATT server.
	    // ACTION_GATT_SERVICES_DISCOVERED: discovered GATT services.
	    // ACTION_DATA_AVAILABLE: received data from the device.  This can be a result of read
	    //                        or notification operations.
	    private final BroadcastReceiver mGattUpdateReceiver = new BroadcastReceiver() {
	        @Override
	        public void onReceive(Context context, Intent intent) {
	            final String action = intent.getAction();
	            if (BluetoothLeService.ACTION_GATT_CONNECTED.equals(action)) {
	                mConnected = true;
	                //updateConnectionState("CONNECTED");
	                //invalidateOptionsMenu();
	            } else if (BluetoothLeService.ACTION_GATT_DISCONNECTED.equals(action)) {
	                mConnected = false;
	                //updateConnectionState("DISCONNECTED");
	                //invalidateOptionsMenu();
	               // clearUI();
	            } else if (BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED.equals(action)) {
	                // Show all the supported services and characteristics on the user interface.
	                displayGattServices(mBluetoothLeService.getSupportedGattServices());
	            } else if (BluetoothLeService.ACTION_DATA_AVAILABLE.equals(action)) {
	              //  displayData(intent.getStringExtra(BluetoothLeService.EXTRA_DATA));
	            }
	        }
	    };
	    
	    
	private static IntentFilter makeGattUpdateIntentFilter() {
	    final IntentFilter intentFilter = new IntentFilter();
	    intentFilter.addAction(BluetoothLeService.ACTION_GATT_CONNECTED);
	    intentFilter.addAction(BluetoothLeService.ACTION_GATT_DISCONNECTED);
	    intentFilter.addAction(BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED);
	    intentFilter.addAction(BluetoothLeService.ACTION_DATA_AVAILABLE);
	    return intentFilter;
	}
}
